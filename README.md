# 3D evolution of tectonic deformation as driver of seismic hazard in the Hellenic arc

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="90">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Mainz.png?inline=false" width="120">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/LMU.png?inline=false" width="90">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/TUM.png?inline=false" width="90">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="90">,<img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Sorbonne-Univ.png?inline=false" width="90">

**Codes:** [pTatin3D](https://gitlab.com/cheese5311126/codes/ptatin3d/ptatin3d)

**Target EuroHPC Architectures:** LUMI-G, LUMI-C 

**Type:** Basic research

## Description

Dynamic rupture propagation of an earthquake and whether it will potentially
cause a tsunami depends not only on the fault properties but also largely on the
long term tectonic structure of the crust and on the heterogeneity of the state of
stress related to the long-term tectonic evolution. Concerning subduction
earthquakes fault slip and inelastic deformation in the frontal wedge are
intensely debated causes of large coseismic seafloor displacement and resulting
tsunami heights of megathrust earthquakes. The Mw 8.3 AD 365 Crete
earthquake, which resulted in a tsunami that destroyed cities and drowned
thousands of people in coastal regions from the Nile Delta to modern-day
Dubrovnik did not occur on the subduction interface but on a splay fault. The
recurrence of earthquakes happening on these secondary faults is poorly
understood, and a better understanding of the long-term context that leads to
their formation would allow such faults to be identified in the field, subsequently
mapped and accounted for in seismic and tsunami hazard assessment.

<img src="SC8.2.png" width="800">

**Figure 3.13.1.** Numerical simulation of fault evolution with pTatin3D.

## Expected results

Analyze the state of stress and geomorphologic signature of these potential tsunamigenic splay faults
which will help to identify locality of other potential splay faults along the arc. The complex, self consistent state of stress
obtained at the last stage will be used as an input to the rupture propagation code(s) (see SC1.2). The entire evolution of the
upper plate is of interest for many geologists in Europe, who work on the Aegean, and therefore the simulation results would
be of interest to many scientists outside of this project. The approach developed in this case study will be used for other
regional case studies in the future.